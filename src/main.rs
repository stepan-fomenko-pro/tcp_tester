use std::env;
use std::ops::Add;
use std::fmt;
use rand::Rng;
use tokio::io::{AsyncWriteExt, Result};
use tokio::net::TcpStream;
use tokio::sync::{mpsc, oneshot};
use tokio::time::{Duration, sleep};

use Command::Decrement;

enum Command {
    Decrement
}

struct ConnectionStatistic {
    successes: u32,
    fails: u32
}

impl ConnectionStatistic {
    fn origin() -> ConnectionStatistic {
        ConnectionStatistic{ successes: 0, fails: 0 }
    }

    fn update<T>(&mut self, result: Result<T>) {
        if result.is_ok() {
            self.successes += 1;
        } else {
            self.fails += 1;
        }
    }
}

impl Add for ConnectionStatistic {
    type Output = Self;

    fn add(self, other: Self) -> Self {
        ConnectionStatistic {
            successes: self.successes + other.successes,
            fails: self.fails + other.fails
        }
    }
}

impl fmt::Display for ConnectionStatistic {
    // This trait requires `fmt` with this exact signature.
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        // Write strictly the first element into the supplied output
        // stream: `f`. Returns `fmt::Result` which indicates whether the
        // operation succeeded or failed. Note that `write!` uses syntax which
        // is very similar to `println!`.
        write!(f, "successes: {}, fails: {}", self.successes, self.fails)
    }
}

async fn connect_task(host_port: &str, _: usize) -> Result<()> {
    let mut stream = TcpStream::connect(host_port).await?;

    // stream.write(&[0; 256]).await?;

    let keep_alive_time = rand::thread_rng().gen_range(100..10000);
    sleep(Duration::from_micros(keep_alive_time)).await;

    stream.shutdown().await?;

    Ok(())
}

async fn connect_loop(host_port: String, cmd_tx: mpsc::Sender<(Command, oneshot::Sender<usize>)>) -> Result<ConnectionStatistic> {
    let mut statistic = ConnectionStatistic{ successes: 0, fails: 0 };

    loop {
        let (resp_tx, resp_rx) = oneshot::channel();

        if cmd_tx.send((Decrement, resp_tx)).await.is_err() {
            break
        }

        match resp_rx.await {
            Ok(res) => { statistic.update(connect_task(&host_port, res).await); }
            Err(_) => { break }
        }
    }

    Ok(statistic)
}

async fn counter_task(mut remaining_count: usize, mut cmd_rx: mpsc::Receiver<(Command, oneshot::Sender<usize>)>) -> Result<()> {
    while let Some((cmd, response)) = cmd_rx.recv().await {
        match cmd {
            Decrement => {
                let prev = remaining_count;
                remaining_count -= 1;
                response.send(prev).unwrap();
                if remaining_count == 0 {
                    cmd_rx.close();
                    break;
                }
            }
        }
    }

    Ok(())
}

#[tokio::main]
async fn main() -> Result<()> {
    let args: Vec<String> = env::args().collect();

    let host = args[1].as_str();
    let port = args[2].as_str();
    let host_port = host.to_owned() + ":" + port;

    let remaining_count = args[3].as_str().parse::<usize>().unwrap();

    let (cmd_tx, cmd_rx) = mpsc::channel::<(Command, oneshot::Sender<usize>)>(100);

    // Spawn a task to manage the counter
    tokio::spawn(counter_task(remaining_count, cmd_rx));

    let mut join_handles = vec![];

    // Spawn tasks that will send the increment command.
    for _ in 0..5000 {
        join_handles.push(tokio::spawn(connect_loop(host_port.clone(), cmd_tx.clone())));
    }

    // Wait for all tasks to complete
    let mut statistics = ConnectionStatistic::origin();
    for join_handle in join_handles.drain(..) {
        statistics = statistics + join_handle.await.unwrap().unwrap();
    }
    println!("{}", statistics);

    Ok(())
}